// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    api: 'http://localhost:3000/',
    vimeo: 'https://vimeo.com/api/v2/video/',
    audiodb: 'https://theaudiodb.com/api/v1/json/1/search.php?s=',
    content: {
        global: {
            menu: [
                {
                    'title': 'Matt Brock',
                    'state': ''
                }
            ],
            menuAuth: [
                {
                    'title': 'Dashboard',
                    'state': '/dashboard',
                    'icon': 'home'
                },
                {
                    'title': 'Account',
                    'state': '/account',
                    'icon': 'cog'
                },
                {
                    'title': 'Content',
                    'state': '/cms/content',
                    'icon': 'eye'
                },
                {
                    'title': 'Videos',
                    'state': '/cms/videos',
                    'icon': 'video'
                },
                {
                    'title': 'Photos',
                    'state': '/cms/photos',
                    'icon': 'image'
                },
                {
                    'title': 'Reviews',
                    'state': '/cms/reviews',
                    'icon': 'check'
                }
            ],
            menuAuthNav: [
                {
                    'title': 'Account',
                    'state': '/account',
                    'icon': 'cog'
                },
                {
                    'title': 'Dashboard',
                    'state': '/dashboard',
                    'icon': 'home'
                }
            ],
            prefooter: {
                content: [
                    {
                        title: 'Mandy',
                        link: 'https://www.mandy.com/crew/profile/diceperson',
                        image: 'logo-my-1.png'
                    },
                    {
                        title: 'Shooting People',
                        link: 'https://shootingpeople.org/cards/diceperson',
                        image: 'logo-sp-1.png'
                    },
                    {
                        title: 'Soundcloud',
                        link: 'https://soundcloud.com/dicepeople',
                        image: 'logo-sc-1.png'
                    },
                    {
                        title: 'YouTube',
                        link: 'https://www.youtube.com/channel/UCXZqJ6CV5GmMF0xg0ULvcOg',
                        image: 'logo-yt-1.png'
                    },
                    {
                        title: 'Dicepeple',
                        link: 'https://dicepeople.com',
                        image: 'logo-dp-2.png'
                    }
                ]
            },
            footer: {
                body: {
                    text: 'freepowder',
                    link: '',
                    brand: ''
                }
            },
            social: [
                {
                    icon: 'facebook',
                    link: 'https://https://www.facebook.com/diceperson',
                },
                {
                    icon: 'twitter',
                    link: ' https://twitter.com/_mattbrock',
                }
            ]
        },
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
