import { TestBed, inject } from '@angular/core/testing';
import { AuthService } from './auth-jwt.service';
import { LocalStorageService } from 'ngx-webstorage';

describe('Shared Module - auth-jwt-service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService, LocalStorageService ]
    });
  });
  it('Shared Module - auth-jwt-service - should be creating the service',
    inject([AuthService], (service: AuthService) => {
      expect(service).toBeTruthy();
  }));
  it('Shared Module - auth-jwt-service - should have getToken method',
    inject([AuthService], (service: AuthService) => {
      expect(service.getToken).toBeDefined();
    }));
  it('Shared Module - auth-jwt-service - should have setAuth method',
    inject([AuthService], (service: AuthService) => {
      expect(service.setAuth).toBeDefined();
    }));
  it('Shared Module - auth-jwt-service - should have destroyAuth method',
    inject([AuthService], (service: AuthService) => {
      expect(service.destroyAuth).toBeDefined();
    }));
  it('Shared Module - auth-jwt-service - should have isLoggedIn method',
    inject([AuthService], (service: AuthService) => {
      expect(service.isLoggedIn).toBeDefined();
    }));
  it('Shared Module - auth-jwt-service - should have getUser method',
    inject([AuthService], (service: AuthService) => {
      expect(service.getUser).toBeDefined();
    }));
  it('Shared Module - auth-jwt-service - setAuth method should set localstorage user and token',
    inject([AuthService, LocalStorageService], (service: AuthService, localStorage: LocalStorageService) => {
      const user = {_id: '5a56c5df49337d08513fcbc4',
        displayName: 'alvaro retana',
        provider: 'local',
        created: '2018-01-11T02:03:11.471Z',
        roles: ['user', 'admin'],
        profileImageURL: 'modules/users/client/img/profile/default.png',
        email: 'arfs@ecolove.com',
        lastName: 'retana',
        firstName: 'alvaro'
      };
      const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTU2YzVkZjQ5MzM3ZDA4NTEzZmNiYzQiLCJka' +
        'XNwbGF5TmFtZSI6ImFsdmFybyByZXRhbmEiLCJwcm92aWRlciI6ImxvY2FsIiwiX192IjowLCJjcmVhdGVkIjoiMjAxOC0wMS0xMV' +
        'QwMjowMzoxMS40NzFaIiwicm9sZXMiOlsidXNlciIsImFkbWluIl0sInByb2ZpbGVJbWFnZVVSTCI6Im1vZHVsZXMvdXNlcnMvY2xpZ' +
        'W50L2ltZy9wcm9maWxlL2RlZmF1bHQucG5nIiwiZW1haWwiOiJhcmZzQGVjb2xvdmUuY29tIiwibGFzdE5hbWUiOiJyZXRhbmEiLCJmaX' +
        'JzdE5hbWUiOiJhbHZhcm8iLCJpYXQiOjE1MTY0Njk4ODQsImV4cCI6MTcyOTQ5NDU4ODR9.sA6fLQgPWiIKgnpEOfXvs9dvZLEWLMS_1eMbgwJvoUg';
      service.setAuth(user, token).then(() => {
        expect(localStorage.retrieve('user')).toEqual(user);
        expect(localStorage.retrieve('authenticationToken')).toEqual(token);
      });
    }));
  it('Shared Module - auth-jwt-service - setAuth method should set localstorage user and token',
    inject([AuthService, LocalStorageService], (service: AuthService, localStorage: LocalStorageService) => {
      service.destroyAuth().subscribe(() => {
        expect(localStorage.retrieve('user')).toEqual(null);
        expect(localStorage.retrieve('authenticationToken')).toEqual(null);
      });
    }));
});
