import { TestBed, inject } from '@angular/core/testing';
import { AuthService } from './auth-jwt.service';
import { LocalStorageService } from 'ngx-webstorage';
import { UserRouteAccessService } from './user-route-access-service';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../user.service/user.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Shared Module - user-route-access-service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [AuthService, UserService, UserRouteAccessService, LocalStorageService ]
    });
  });
  it('Shared Module - user-route-access-service - should be creating the service',
    inject([UserRouteAccessService], (service: UserRouteAccessService) => {
      expect(service).toBeTruthy();
  }));
  
});
