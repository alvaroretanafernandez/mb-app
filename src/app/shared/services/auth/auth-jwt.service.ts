import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LocalStorageService } from 'ngx-webstorage';
import * as jwt_decode from 'jwt-decode';
import { Subject } from 'rxjs';

@Injectable()
export class AuthService {
  private user: any;
  private authenticationState = new Subject<any>();
  
  constructor(private $localStorage: LocalStorageService) {
  }
  
  /**
   * get Token
   * @returns {any}
   */
  getToken() {
    return this.$localStorage.retrieve('authenticationToken');
  }
  
  /**
   * set Auth
   * @param user
   * @param token
   * @returns {Promise<any>}
   */
  setAuth(user, token): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      if (!!token && !!user) {
        this.$localStorage.store('authenticationToken', token);
        this.$localStorage.store('user', user);
        this.user = user;
        this.authenticationState.next(this.user);
        resolve();
      } else {
        reject();
      }
    });
  }
  /**
   * destroy Auth
   * @returns {Observable|"../../Observable".Observable|"../../../Observable".Observable}
   */
  destroyAuth(): Observable<any> {
    return new Observable((observer) => {
      this.$localStorage.clear('authenticationToken');
      this.$localStorage.clear('user');
      this.user = null;
      this.authenticationState.next(this.user);
      observer.complete();
    });
  }

  /**
   * check token is expired
   * @returns boolean
   */
  isLoggedIn(): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      const value = this.$localStorage.retrieve('authenticationToken');
      if (value !== null) {
        const storedToken = jwt_decode(value);
        if (!storedToken || Date.now() / 1000 > storedToken['exp']) {
          this.$localStorage.clear('user');
          this.$localStorage.clear('authenticationToken');
          resolve(false);
        }
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  getUser(): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      const value = this.$localStorage.retrieve('authenticationToken');
      if (value !== null) {
        const storedToken = jwt_decode(value);
        if (!storedToken || Date.now() / 1000 > storedToken['exp']) {
          this.$localStorage.clear('user');
          this.$localStorage.clear('authenticationToken');
          resolve(null);
        }
        resolve(this.$localStorage.retrieve('user'));
      } else {
        resolve(null);
      }
    });
  }
  
  getAuthenticationState(): Observable<any> {
    return this.authenticationState.asObservable();
  }

  hasAnyAuthority(authorities: string[]): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      const user = this.$localStorage.retrieve('user');
      let isAllowed = false;
      for (let i = 0; i < authorities.length; i++) {
        if (user['roles'].includes(authorities[i])) {
          isAllowed = true;
        }
      }
      resolve(isAllowed);
    });
  }
  isAllowed(allowedRoles: string[]): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      let isAllowed = false;
      const value = this.$localStorage.retrieve('authenticationToken');
      if (value !== null) {
        const storedToken = jwt_decode(value);
        if (!storedToken || Date.now() / 1000 > storedToken['exp']) {
          this.$localStorage.clear('user');
          this.$localStorage.clear('authenticationToken');
          resolve(null);
        }
        const user = this.$localStorage.retrieve('user');
        for (let i = 0; i < allowedRoles.length; i++) {
          if (user['roles'].includes(allowedRoles[i])) {
            isAllowed = true;
          }
        }
        resolve(isAllowed);
      } else {
        resolve(isAllowed);
      }
    });
  }
  
  
}
