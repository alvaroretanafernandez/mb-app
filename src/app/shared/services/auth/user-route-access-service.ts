import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth-jwt.service';
import { Observable } from 'rxjs';
import { UserService } from '../user.service/user.service';

@Injectable()
export class UserRouteAccessService implements CanActivate {
    auth;
    
    constructor(private router: Router, auth: AuthService, private userService: UserService) {
        this.auth = auth;
    }
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        const allowedRoles = route.data['role'];
        return this.checkIsAllowed(allowedRoles, state.url);
    }
    checkIsAllowed(roles: string[], url: string): Promise<boolean> {
        return new Promise<any>((resolve, reject) => {
            // from server
            this.userService.getMe()
                .subscribe(
                    (me) => {
                        let isAllowed;
                        for (let i = 0; i < roles.length; i++) {
                            if (me['roles'].includes(roles[i])) {
                                isAllowed = true;
                            }
                        }
                        resolve(isAllowed);
                    },
                    (error) => {
                        console.log(error);
                        reject(false);
                    });

            // FROM STORAGE OR COOKIE
            // this.auth.isAllowed(roles).then((isAllowed) => {
            //   resolve(isAllowed)
            // })
        });
    }
}
