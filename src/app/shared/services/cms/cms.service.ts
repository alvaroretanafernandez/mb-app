import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CmsService {
    constructor(public http: HttpClient) { }
    getContent(): Observable<any> {
        return this.http.get(environment.api + 'api/cms/content/');
    }
    updateContent(content: any, id): Observable<any> {
        return this.http.put(environment.api + 'api/cms/content/' + id, content);
    }
    getVideos(): Observable<any> {
        return this.http.get(environment.api + 'api/cms/videos/');
    }
    updateVideo(video: any, id): Observable<any> {
        return this.http.put(environment.api + 'api/cms/videos/' + id, video);
    }
    deleteVideo(video: any, id): Observable<any> {
        return this.http.delete(environment.api + 'api/cms/videos/' + id, video);
    }
    createVideo(video: any): Observable<any> {
        return this.http.post(environment.api + 'api/cms/videos/', video);
    }
    getVideoById(id): Observable<any> {
        return this.http.get(environment.api + 'api/cms/videos/' + id);
    }
    getPhotos(): Observable<any> {
        return this.http.get(environment.api + 'api/cms/photos/');
    }
    updatePhoto(photo: any, id): Observable<any> {
        return this.http.put(environment.api + 'api/cms/photos/' + id, photo);
    }
    deletePhoto(photo: any, id): Observable<any> {
        return this.http.delete(environment.api + 'api/cms/photos/' + id, photo);
    }
    
    getReviews(): Observable<any> {
        return this.http.get(environment.api + 'api/cms/reviews/');
    }
    updateReview(review: any, id): Observable<any> {
        return this.http.put(environment.api + 'api/cms/reviews/' + id, review);
    }
    deleteReview(review: any, id): Observable<any> {
        return this.http.delete(environment.api + 'api/cms/reviews/' + id, review);
    }
    createReview(review: any): Observable<any> {
        return this.http.post(environment.api + 'api/cms/reviews/', review);
    }
    
}
