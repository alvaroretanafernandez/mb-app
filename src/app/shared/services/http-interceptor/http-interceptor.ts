import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from '../auth/auth-jwt.service';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
    vimeo = environment.vimeo;
    audiodb = environment.audiodb;
    constructor(public auth: AuthService, private toastr: ToastrService, private router: Router) {
    }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // console.log('intercepted request ... ');
        req.headers.delete('authorization');
        req = req.clone({
            headers: req.headers.delete('authorization')
        });
        const isVimeo = req.url.includes(this.vimeo);
        const isAudidb = req.url.includes(this.audiodb);
        if (this.auth.getToken() && !isVimeo && !isAudidb) {
            req = req.clone({
                headers: req.headers.set('authorization', 'Bearer ' + this.auth.getToken())
            });
        }
        // console.log('Sending request with new header now ...');
        return next.handle(req)
            .pipe(
                catchError((error, caught) => {
                    // intercept the respons error and displace it to the console
                    return this.handleAuthError(error);
                    // return of(error);
                }) as any);
    }
    /**
     * manage errors
     * @param err
     * @returns {any}
     */
    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        if (err.status === 422) {
            // navigate /delete cookies or whatever
            console.log('handled error ' + err.status);
            // this.router.navigate([`/login`]);
            return throwError(err);
        }
        // handle your auth error or rethrow
        if (err.status === 401) {
            // navigate /delete cookies or whatever
            console.log('handled error ' + err.status);
            this.router.navigate([`/login`]);
            return of(err.message);
        }
        if (err.status === 403) {
            // navigate /delete cookies or whatever
            console.log('handled error ' + err.status);
            this.toastr.error('User not allowed', err.status + ' ' + err.statusText, { disableTimeOut: true });
            // this.router.navigate([`/login`]);
            return throwError(err.message);
        }
        throwError(err);
    }
}
