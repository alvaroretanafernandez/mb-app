import { TestBed, inject, async } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../../../environments/environment';

describe('Shared Module - user-service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, HttpClientTestingModule],
      providers: [UserService]
    });
  });

  it('Shared Module - user-service - should be creating the service', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));
  it('Shared Module - user-service - should have register method',
    inject([UserService], (service: UserService) => {
      expect(service.register).toBeDefined();
    }));
  it('Shared Module - user-service - should have login method',
    inject([UserService], (service: UserService) => {
      expect(service.login).toBeDefined();
    }));
  it('Shared Module - user-service - should have logout method',
    inject([UserService], (service: UserService) => {
      expect(service.logout).toBeDefined();
    }));

  it('Shared Module - user-service - login should expect POST api/auth/signin', async(inject([UserService, HttpTestingController],
    (service: UserService, backend: HttpTestingController) => {
      const data = {
        email: 'email',
        password: 'password',
      };
      service.login(data).subscribe();
      backend.expectOne({
        url: environment.api + 'api/auth/signin',
        method: 'POST'
      });
    })));
  it('Shared Module - user-service - register should expect POST api/auth/signup', async(inject([UserService, HttpTestingController],
  (service: UserService, backend: HttpTestingController) => {
    const data = {
      email: 'email',
      password: 'password',
      username: 'test',
      firstName: 'test',
      lastName: 'test',
    };
    service.register(data).subscribe();
    backend.expectOne({
      url: environment.api + 'api/auth/signup',
      method: 'POST'
    });
  })));
  it('Shared Module - user-service - logout should expect GET api/auth/signout', async(inject([UserService, HttpTestingController],
    (service: UserService, backend: HttpTestingController) => {
      service.logout().subscribe();
      backend.expectOne({
        url: environment.api + 'api/auth/signout',
        method: 'GET'
      });
    })));
});
