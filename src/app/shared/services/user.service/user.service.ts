import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable()
export class UserService {
  constructor(public http: HttpClient) { }

  /**
   * User Login
   * @param credentials
   * @returns {Observable<Object>}
   */
  login(credentials: any): Observable<any> {
    const data = {
      email: credentials.email,
      password: credentials.password,
    };
    return this.http.post(environment.api + 'api/auth/signin', data);
  }

  /**
   * User Register
   * @param credentials
   * @returns {Observable<Object>}
   */
  register(credentials: any): Observable<any> {
    const data = {
      firstName: credentials.firstName,
      lastName: credentials.lastName,
      email: credentials.email,
      username: credentials.username,
      password: credentials.password,
    };
    return this.http.post(environment.api + 'api/auth/signup', data);
  }

  /**
   * User Logout
   * @returns {Observable<any>}
   */
  logout(): Observable<any> {
    return this.http.get(environment.api + 'api/auth/signout');
  }
  getUsers(): Observable<any> {
    return this.http.get(environment.api + 'api/users');
  }
  getMe(): Observable<any> {
    return this.http.get(environment.api + 'api/users/me');
  }
    changePassword(data): Observable<any> {
        return this.http.post(environment.api + 'api/users/password',data);
    }
  updateMe(data): Observable<any> {
     return this.http.put(environment.api + 'api/users', data);
  }
}
