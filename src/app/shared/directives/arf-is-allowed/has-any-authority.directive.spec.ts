
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HasAnyAuthorityDirective } from './has-any-authority.directive';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { LocalStorageService } from 'ngx-webstorage';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '../../services/auth/auth-jwt.service';

@Component({
  template: `<div *arfHasAnyAuthority="'admin'">
              <a [routerLink]="'./admin'" routerLinkActive="active" [routerLinkActiveOptions]="{exact: true}">
                <img src="assets/avatar.png" class="header-profile-image" />ADMIN
              </a>
            </div>`
})
class TestAuthorityComponent {
}

const user = {_id: '5a56c5df49337d08513fcbc4',
  displayName: 'alvaro retana',
  provider: 'local',
  created: '2018-01-11T02:03:11.471Z',
  roles: ['user', 'admin'],
  profileImageURL: 'modules/users/client/img/profile/default.png',
  email: 'arfs@ecolove.com',
  lastName: 'retana',
  firstName: 'alvaro'
};

const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTU2YzVkZjQ5MzM3ZDA4NTEzZmNiYzQiLCJka' +
  'XNwbGF5TmFtZSI6ImFsdmFybyByZXRhbmEiLCJwcm92aWRlciI6ImxvY2FsIiwiX192IjowLCJjcmVhdGVkIjoiMjAxOC0wMS0xMV' +
  'QwMjowMzoxMS40NzFaIiwicm9sZXMiOlsidXNlciIsImFkbWluIl0sInByb2ZpbGVJbWFnZVVSTCI6Im1vZHVsZXMvdXNlcnMvY2xpZ' +
  'W50L2ltZy9wcm9maWxlL2RlZmF1bHQucG5nIiwiZW1haWwiOiJhcmZzQGVjb2xvdmUuY29tIiwibGFzdE5hbWUiOiJyZXRhbmEiLCJmaX' +
  'JzdE5hbWUiOiJhbHZhcm8iLCJpYXQiOjE1MTY0Njk4ODQsImV4cCI6MTcyOTQ5NDU4ODR9.sA6fLQgPWiIKgnpEOfXvs9dvZLEWLMS_1eMbgwJvoUg';

describe('Shared Module - has-any-authority-directive', () => {
  let component: TestAuthorityComponent;
  let fixture: ComponentFixture<TestAuthorityComponent>;
  let inputEl: any;
  let $localStorage: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [TestAuthorityComponent, HasAnyAuthorityDirective],
      providers: [
        LocalStorageService,
        AuthService
      ]
    });
  });
  beforeEach(() => {
    fixture = TestBed.createComponent(TestAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    $localStorage = TestBed.get(LocalStorageService);
    $localStorage.store('authenticationToken', token);
    $localStorage.store('user', user);
    inputEl = fixture.debugElement.query(By.css('header-profile-image'));
  });
  afterEach(() => {
    $localStorage = TestBed.get(LocalStorageService);
    $localStorage.clear('authenticationToken');
    $localStorage.clear('user');
  });
  it('Shared Module - has-any-authority-directive - when Admin user should show the link', () => {
    expect(inputEl).toBeDefined();
  });
  it('Shared Module - has-any-authority-directive - when Guest user should not show the link', () => {
    $localStorage = TestBed.get(LocalStorageService);
    $localStorage.clear('authenticationToken');
    $localStorage.clear('user');
    expect(inputEl).toBeNull();
  });
});


