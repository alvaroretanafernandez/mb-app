import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../services/auth/auth-jwt.service';

/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *arfHasAnyAuthority="'admin'">...</some-element>
 *
 *     <some-element *arfHasAnyAuthority="['admin', 'user']">...</some-element>
 * ```
 */
@Directive({
    selector: '[arfHasAnyAuthority]'
})
export class HasAnyAuthorityDirective {

    private authorities: string[];

    constructor(private auth: AuthService, private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {
    }

    @Input()
    set arfHasAnyAuthority(value: string|string[]) {
        this.authorities = typeof value === 'string' ? [ <string> value ] : <string[]> value;
        this.updateView();
        // Get notified each time authentication state changes.
        this.auth.getAuthenticationState().subscribe((user) => this.updateView());
    }

    private updateView(): void {
      this.auth.isAllowed(this.authorities).then((result) => {
          this.viewContainerRef.clear();
            if (result) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            }
        });
    }
}
