import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafePipe } from './pipes/safe.pipe';
import { PasswordStrengthBarComponent } from './components/password/password-strength-bar.component';
import { ArfToastrComponent } from './components/toastr/arf-toastr.component';
import { HasAnyAuthorityDirective } from './directives/arf-is-allowed/has-any-authority.directive';
import { SidebarMenuComponent } from './components/layout/sidebar-menu/sidebar-menu.component';
import { LayoutComponent } from './components/layout/layout.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PopupComponent } from './components/popup/popup.component';
import { AlertPopupComponent } from './components/alert-popup/alert-popup.component';
import { SoundcloudComponent } from '../home/components/content-soundcloud/soundcloud.component';
import { PreFooterComponent } from '../home/components/content-prefooter/prefooter.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    declarations: [
        HasAnyAuthorityDirective,
        PasswordStrengthBarComponent,
        ArfToastrComponent,
        SidebarMenuComponent,
        LayoutComponent,
        PopupComponent,
        SoundcloudComponent,
        AlertPopupComponent,
        PreFooterComponent
    ],
    exports: [
        CommonModule,
        RouterModule,
        FormsModule,
        ReactiveFormsModule,
        HasAnyAuthorityDirective,
        PasswordStrengthBarComponent,
        ArfToastrComponent,
        SidebarMenuComponent,
        LayoutComponent,
        PopupComponent,
        SoundcloudComponent,
        AlertPopupComponent,
        PreFooterComponent
    ],
    entryComponents: [PopupComponent, AlertPopupComponent]
})
export class SharedModule {
}
