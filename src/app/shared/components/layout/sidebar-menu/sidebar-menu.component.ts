import { Component, Input, OnInit } from '@angular/core';
import { environment } from '../../../../../environments/environment';

@Component({
    selector: 'arf-sidebar-menu',
    templateUrl: './sidebar-menu.component.html',
    styleUrls: ['./sidebar-menu.component.scss']
})
export class SidebarMenuComponent implements OnInit {
    @Input() user;
    menuAuth: any = environment.content.global.menuAuth;
    constructor() {
    }
    ngOnInit() {
    }
}
