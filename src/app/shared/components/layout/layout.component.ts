import { Component, Input, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'arf-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {
    @Input() user;
    constructor(private $localStorage: LocalStorageService) { }
    ngOnInit() {
      this.user = this.$localStorage.retrieve('user');
    }
}
