import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { PasswordStrengthBarComponent } from './password-strength-bar.component';
import { SharedModule } from '../../shared.module';


describe('Shared Module - password-strength-bar-component', () => {
  let component: PasswordStrengthBarComponent;
  let fixture: ComponentFixture<PasswordStrengthBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      declarations: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordStrengthBarComponent);
    component = fixture.componentInstance;
    spyOn(component, 'getColor').and.returnValue({idx: 2, col: '#dedede'});
    fixture.detectChanges();
  });

  it('Shared Module - password-strength-bar-component - should create the component', () => {
    expect(component).toBeTruthy();
  });
  it('Shared Module - password-strength-bar-component - should have colors defined in the component', () => {
    fixture = TestBed.createComponent(PasswordStrengthBarComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.colors).toBeDefined();
  });
  it('Shared Module - password-strength-bar-component - should have @Input passwordToCheck defined in the component', () => {
    fixture = TestBed.createComponent(PasswordStrengthBarComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.passwordToCheck).toBeFalsy();
  });
  it('Shared Module - password-strength-bar-component - should have @Input passwordToCheck defined in the component', () => {
    fixture = TestBed.createComponent(PasswordStrengthBarComponent);
    component.passwordToCheck = 'MexicoRocks0!';
    fixture.detectChanges();
    expect(component.getColor).toHaveBeenCalled();
  });
});
