import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
    selector: 'arf-alert-popup',
    templateUrl: './alert-popup.component.html',
    styleUrls: ['./alert-popup.component.scss']
})
export class AlertPopupComponent implements OnInit {
    display: any;
    @Output() public onUserConfirm = new EventEmitter();
    constructor(public bsModalRef: BsModalRef) {
    }
    ngOnInit() {
    }
    closeModal() {
        this.bsModalRef.hide();
    }
    onConfirm() {
        this.onUserConfirm.emit(true);
        this.bsModalRef.hide();
    }

}
