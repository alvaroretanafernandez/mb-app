import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'arf-popup',
    templateUrl: './popup.component.html',
    styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
    display: any;
    iframe = false;
    sanIframe;
    constructor(public bsModalRef: BsModalRef, public sanitizer: DomSanitizer) {
    }
    ngOnInit() {
        this.sanIframe = this.sanitizer.bypassSecurityTrustResourceUrl(this.display.iframe);
    }
    closeModal() {
        this.bsModalRef.hide();
    }
}
