import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';


@Component({
  selector: 'arf-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user;
  constructor(private router: Router, private authService: AuthService,
              private $localStorage: LocalStorageService) { }

  ngOnInit() {
      this.user = this.$localStorage.retrieve('user');
  }
}
