import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountComponent } from './account.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../../shared/services/user.service/user.service';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { UserDetailsComponent } from '../components/user-details/user-details.component';
import { AccountLinksComponent } from '../components/account-links/account-links.component';

describe('AccountComponent', () => {
  let component: AccountComponent;
  let fixture: ComponentFixture<AccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [SharedModule, RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot(), FormsModule, ReactiveFormsModule],
        declarations: [ AccountComponent, UserDetailsComponent, AccountLinksComponent ],
        providers: [UserService, AuthService, LocalStorageService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
