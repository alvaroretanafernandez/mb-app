import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../shared/services/user.service/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'arf-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    form: FormGroup;
    user;
    error;
    errorMessage;
    
    constructor(private formBuilder: FormBuilder, private userService: UserService, private toastr: ToastrService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'password': ['', [Validators.required]],
            'newPassword': ['', [Validators.required]],
            'confirmPassword': ['', [Validators.required]]
        });
    }
    
    changePassword() {
        this.error = false;
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({ 'required': true });
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        if (this.form.controls['newPassword'].value !== this.form.controls['confirmPassword'].value) {
            this.error = true;
            this.errorMessage = 'Ooops ...New Password and Confirm Password doesnt match!';
            return;
        }
        const credentials = {
            password: this.form.controls['password'].value,
            newPassword: this.form.controls['newPassword'].value,
            confirmPassword: this.form.controls['confirmPassword'].value,
        };
        this.userService.changePassword(credentials)
            .subscribe(
                (data) => {
                    this.toastr.success('You password has been changed!', 'Change password success!');
                    this.form.reset();
                },
                (error) => {
                    console.log(error);
                    if (error.status === 422) {
                        this.error = true;
                        this.errorMessage = error.error.message;
                    }
                    // this.toastr.error('Login Error!', 'You are not logged in');
                    // this.authenticationError = true;
                });
    }
}
