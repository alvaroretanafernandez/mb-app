import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangePasswordComponent } from './change-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../../shared/services/user.service/user.service';
import { LocalStorageService } from 'ngx-webstorage';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { TooltipModule } from 'ngx-bootstrap';

describe('ChangePasswordComponent', () => {
    let component: ChangePasswordComponent;
    let fixture: ComponentFixture<ChangePasswordComponent>;
    
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot(), FormsModule, ReactiveFormsModule, TooltipModule.forRoot()],
            declarations: [ChangePasswordComponent],
            providers: [ToastrService, UserService, AuthService, LocalStorageService]
        })
            .compileComponents();
    }));
    
    beforeEach(() => {
        fixture = TestBed.createComponent(ChangePasswordComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
