import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'arf-account-home',
  templateUrl: './account-home.component.html',
  styleUrls: ['./account-home.component.scss']
})
export class AccountHomeComponent implements OnInit {
  user;
  constructor(private router: Router, private authService: AuthService,
              private $localStorage: LocalStorageService,
              private userService: UserService) { }

  ngOnInit() {
      this.user = this.$localStorage.retrieve('user');
  }

}
