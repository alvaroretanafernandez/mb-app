import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from 'ngx-webstorage';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { ToastrService } from 'ngx-toastr';

export interface User {
    created: string;
    email: string;
    firstName: string;
    lastName: string;
    roles: string[];
    updated: string;
    username: string;
    _id: string;
}


@Component({
  selector: 'arf-my-details',
  templateUrl: './my-details.component.html',
  styleUrls: ['./my-details.component.scss']
})
export class MyDetailsComponent implements OnInit {
    form: FormGroup;
    user: User;
    isEditAccount = false;
    error;
    errorMessage;
    constructor(
        private formBuilder: FormBuilder,
        private toastr: ToastrService,
        private authService: AuthService,
        private userService: UserService,
        private $localStorage: LocalStorageService) { }
    ngOnInit() {
        this.user = this.$localStorage.retrieve('user');
        this.form = this.formBuilder.group({
            'firstName': [ this.user['firstName'] , [Validators.required]],
            'lastName': [this.user['lastName'], [Validators.required]],
            'userName': [this.user['username'], [Validators.required]],
            'email': [{value: this.user['email'], disabled: true}, [Validators.required]]
        });
    }
    updateAccount() {
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({ 'required': true });
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        this.error = false;
        this.userService.updateMe({
            firstName: this.form.controls['firstName'].value,
            lastName: this.form.controls['lastName'].value,
            username: this.form.controls['userName'].value,
        }).subscribe((data) => {
            this.authService.setAuth(data.user, data.token)
                .then(() => {
                    this.user = this.$localStorage.retrieve('user');
                    this.isEditAccount = false;
                    this.toastr.success('You details are now up to date!', 'Update user success!' , { disableTimeOut: true });
                });
        }, (error) => {
            console.log(error);
            if (error.status === 422) {
                this.error = true;
                this.errorMessage = error.error.message;
            }
        });
    }
}
