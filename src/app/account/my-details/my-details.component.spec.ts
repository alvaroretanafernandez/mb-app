import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyDetailsComponent } from './my-details.component';
import { SharedModule } from '../../shared/shared.module';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { UserDetailsComponent } from '../components/user-details/user-details.component';
import { AccountLinksComponent } from '../components/account-links/account-links.component';

const mockUser = {_id: '5a56c5df49337d08513fcbc4',
    displayName: 'alvaro retana',
    provider: 'local',
    created: '2018-01-11T02:03:11.471Z',
    roles: ['user', 'admin'],
    profileImageURL: 'modules/users/client/img/profile/default.png',
    email: 'arfs@ecolove.com',
    lastName: 'retana',
    firstName: 'alvaro'
};


describe('MyDetailsComponent', () => {
    let component: MyDetailsComponent;
    let fixture: ComponentFixture<MyDetailsComponent>;
    let $localStorage;
    
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [SharedModule, RouterTestingModule, HttpClientTestingModule, ToastrModule.forRoot(), FormsModule, ReactiveFormsModule],
            declarations: [MyDetailsComponent],
            providers: [ToastrService, UserService, AuthService, LocalStorageService]
        })
            .compileComponents();
    }));
    
    beforeEach(() => {
        $localStorage = TestBed.get(LocalStorageService);
        $localStorage.store('user', mockUser);
        fixture = TestBed.createComponent(MyDetailsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    
    afterEach(() => {
        $localStorage.clear();
    });
    
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
