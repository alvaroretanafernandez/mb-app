import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'arf-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
  @Input() user;
  constructor() { }

  ngOnInit() {
  }

}
