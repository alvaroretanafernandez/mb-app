import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRouteAccessService } from '../shared/services/auth/user-route-access-service';
import { AccountComponent } from './account/account.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MyDetailsComponent } from './my-details/my-details.component';
import { AccountHomeComponent } from './account-home/account-home.component';

const routes: Routes = [
    { path: '', component: AccountComponent, data: { role: ['admin', 'user'] }, canActivate: [UserRouteAccessService],
        children: [
            { path: '', component: AccountHomeComponent },
            { path: 'change-password',  component: ChangePasswordComponent },
            { path: 'my-details',  component: MyDetailsComponent },
    
            // { path: 'blog',
            //     children: [
            //         { path: 'list', component: AdminBlogListComponent, pathMatch: 'full'},
            //         { path: 'new', component: AdminBlogItemComponent, pathMatch: 'full'},
            //         { path: 'edit/:id', component: AdminBlogItemComponent, pathMatch: 'full', resolve: { article: BlogItemIdResolverService }}
            //     ]
            // },
            // { path: 'vendor', loadChildren: './vendor/vendor.module#VendorModule' },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
