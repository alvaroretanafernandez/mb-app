import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account/account.component';
import { SharedModule } from '../shared/shared.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { MyDetailsComponent } from './my-details/my-details.component';
import { SharedLibModule } from '../shared-lib/shared-lib.module';
import { UserDetailsComponent } from './components/user-details/user-details.component';
import { AccountLinksComponent } from './components/account-links/account-links.component';
import { AccountHomeComponent } from './account-home/account-home.component';

@NgModule({
    imports: [
        CommonModule,
        AccountRoutingModule,
        SharedModule,
        SharedLibModule,
    ],
    declarations: [
        AccountComponent,
        ChangePasswordComponent,
        MyDetailsComponent,
        UserDetailsComponent,
        AccountLinksComponent,
        AccountHomeComponent
    ]
})
export class AccountModule {
}
