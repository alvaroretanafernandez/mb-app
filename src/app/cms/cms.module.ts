import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VideosComponent } from './containers/videos/videos.component';
import { ContentComponent } from './containers/content-form/content.component';
import { CmsComponent } from './containers/cms/cms.component';
import { SharedModule } from '../shared/shared.module';
import { CmsRoutingModule } from './cms-routing.module';
import { SharedLibModule } from '../shared-lib/shared-lib.module';
import { CmsHomeComponent } from './containers/cms-home/cms-home.component';
import { CmsLinksComponent } from './components/cms-links/cms-links.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { VideoFormComponent } from './containers/video-form/video-form.component';
import { UiSwitchModule } from 'ngx-ui-switch';
import { SelectModule } from 'ng2-select';
import { PhotosComponent } from './containers/photos/photos.component';
import { VideoItemComponent } from './components/video-item/video-item.component';
import { PhotoItemComponent } from './components/photo-item/photo-item.component';
import { ReviewsComponent } from './containers/reviews/reviews.component';
import { ReviewItemComponent } from './components/review-item/review-item.component';


@NgModule({
    imports: [
        CommonModule,
        CmsRoutingModule,
        DragDropModule,
        UiSwitchModule,
        SelectModule,
        SharedModule,
        SharedLibModule,
    ],
    declarations: [ VideosComponent, ContentComponent, CmsComponent, CmsHomeComponent, CmsLinksComponent, VideoFormComponent, PhotosComponent, VideoItemComponent, PhotoItemComponent, ReviewsComponent, ReviewItemComponent]
})
export class CmsModule {
}
