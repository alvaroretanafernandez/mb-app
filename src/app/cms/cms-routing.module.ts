import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserRouteAccessService } from '../shared/services/auth/user-route-access-service';
import { VideosComponent } from './containers/videos/videos.component';
import { ContentComponent } from './containers/content-form/content.component';
import { CmsComponent } from './containers/cms/cms.component';
import { CmsHomeComponent } from './containers/cms-home/cms-home.component';
import { VideoFormComponent } from './containers/video-form/video-form.component';
import { PhotosComponent } from './containers/photos/photos.component';
import { ReviewsComponent } from './containers/reviews/reviews.component';

const routes: Routes = [
    {
        path: '',
        component: CmsComponent,
        data: { role: ['admin', 'user'] },
        canActivate: [UserRouteAccessService],
        children: [
            { path: '', component: CmsHomeComponent},
            { path: 'videos',  component: VideosComponent},
            { path: 'videos/add',  component: VideoFormComponent},
            { path: 'videos/:id',  component: VideoFormComponent},
            { path: 'content',  component: ContentComponent},
            { path: 'photos',  component: PhotosComponent},
            { path: 'reviews',  component: ReviewsComponent},
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CmsRoutingModule { }
