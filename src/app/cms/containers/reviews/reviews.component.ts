import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CmsService } from '../../../shared/services/cms/cms.service';
import { ToastrService } from 'ngx-toastr';
import { PhotoItemComponent } from '../../components/photo-item/photo-item.component';
import { ReviewItemComponent } from '../../components/review-item/review-item.component';
import { AlertPopupComponent } from '../../../shared/components/alert-popup/alert-popup.component';
import { BsModalService } from 'ngx-bootstrap';

@Component({
    selector: 'arf-reviews',
    templateUrl: './reviews.component.html',
    styleUrls: ['./reviews.component.scss']
})
export class ReviewsComponent implements OnInit {
    form: FormGroup;
    content: any = {};
    isCreate = true;
    reviews;
    itemToUpdate;
    displayForm = false;
    private modalConfig = {
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true,
    };
    @ViewChildren(ReviewItemComponent) reviewsList: QueryList<ReviewItemComponent>;
    
    constructor(public formBuilder: FormBuilder,
                public cms: CmsService,
                public modalService: BsModalService,
                public toastr: ToastrService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'description': ['', [Validators.required]],
            'title': ['', [Validators.required]]
        });
        this.cms.getContent()
            .subscribe((data) => {
                this.content = data[0];
                this.fetchReviews(false);
            });
    }
    
    fetchReviews(update) {
        this.cms.getReviews()
            .subscribe((data) => {
                this.reviews = data;
                if (update) {
                    this.updateContent();
                }
            });
    }
    
    
    handleForm() {
        const _review = {
            description: this.form.controls['description'].value,
            title: this.form.controls['title'].value
        };
        if (this.isCreate) {
            this.cms.createReview(_review)
                .subscribe((data) => {
                    this.fetchReviews(true);
                });
        } else {
            this.cms.updatePhoto(_review, this.itemToUpdate._id)
                .subscribe(
                    (data) => {
                        this.fetchReviews(true);
                    },
                    (err) => {
                        this.toastr.error('Error updating the review', 'Update review error!', { disableTimeOut: true });
                    }
                );
        }
    }
    
    showCreateForm() {
        this.isCreate = true;
        this.reviewsList.forEach(item => {
            item.isOnEdit = false;
        });
        this.itemToUpdate = null;
        this.form.controls['description'].setValue('');
        this.form.controls['title'].setValue('');
        this.displayForm = true;
    }
    
    showEditForm(item) {
        this.isCreate = false;
        this.reviewsList.forEach(it => {
            it.isOnEdit = true;
            if (item._id === it.item._id) {
                it.isOnEdit = false;
            }
        });
        // return;
        this.itemToUpdate = item;
        this.displayForm = true;
        this.form.controls['description'].setValue(item.description);
        this.form.controls['title'].setValue(item.title);
    }
    
    cancelEdit() {
        this.isCreate = true;
        this.reviewsList.forEach(item => {
            item.isOnEdit = false;
        });
        this.itemToUpdate = null;
        this.displayForm = false;
    }
    
    removeFromDatabase(item) {
        this.cancelEdit();
        const initialState = {
            display: { title: 'Are you sure you want to remove the review  <br/>' + item.title }
        };
        const modal = this.modalService.show(AlertPopupComponent, Object.assign({}, this.modalConfig, { initialState }));
        modal.content.onUserConfirm.subscribe((value) => {
            if (value) {
                this.deleteReview(item);
                return;
            }
        });
    }
    deleteReview(item) {
        this.cms.deleteReview(item, item._id)
            .subscribe((data) => {
                this.fetchReviews(true);
            },(err) => {
                this.toastr.error('Error deleting the review', 'Delete review error!', { disableTimeOut: true });
            });
    }
    updateContent() {
        this.cms.updateContent({
            reviews: this.reviews,
        }, this.content._id).subscribe((data) => {
            this.toastr.success('Reviews content is now up to date!', 'Update content success!', { disableTimeOut: true });
            //  this.cancelEdit();
        }, (error) => {
            console.log(error);
        });
    }
}
