import { Component, OnInit } from '@angular/core';
import { CmsService } from '../../../shared/services/cms/cms.service';
import { FormBuilder } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CdkDragDrop, copyArrayItem, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AlertPopupComponent } from '../../../shared/components/alert-popup/alert-popup.component';

@Component({
    selector: 'arf-videos',
    templateUrl: './videos.component.html',
    styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit {
    videos = [];
    videosAll = [];
    content;
    error;
    errorMessage;
    modalRef: BsModalRef;
    private modalConfig = {
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true,
    };
    constructor(private formBuilder: FormBuilder,
                private cms: CmsService,
                private modalService: BsModalService,
                private toastr: ToastrService) {
    }
    ngOnInit() {
        this.cms.getContent()
            .subscribe((data) => {
                this.content = data[0];
                this.videos = this.content.videos;
            });
        this.fetchVideos();
    }
    onDrop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            this.toastr.warning('Please update your changes!', 'Reorder success!');
        } else {
            if (event.previousContainer.id === 'cdk-drop-list-0' && event.container.id === 'cdk-drop-list-1') {
                this.toastr.warning('Video already exists in the database!', 'Ooops!', { disableTimeOut: true });
                return;
            }
            if (event.previousContainer.id === 'cdk-drop-list-1' && event.container.id === 'cdk-drop-list-0') {
                const itemIsDisplay = this.videos.filter((it) => it.videoId === event.item.data.videoId)[0];
                if (itemIsDisplay) {
                    this.toastr.warning('Video is already in collection!', 'Ooops!');
                    return;
                }
            }
            copyArrayItem(event.previousContainer.data, event.container.data, event.previousIndex, event.currentIndex);
            this.updateOrder();
        }
    }
    removeFromDisplay(item) {
        const filteredItems = this.videos.filter((it) => it.videoId !== item.videoId);
        this.videos = filteredItems;
        const initialState = {
            display: { title: 'Are you sure you want to remove the video ' + item.videoId }
        };
        const modal = this.modalService.show(AlertPopupComponent, Object.assign({}, this.modalConfig, { initialState }));
        modal.content.onUserConfirm.subscribe((value) => {
            if (value) {
               this.updateOrder();
            }
        });
    }
    removeFromDatabase(item) {
        const itemIsDisplay = this.videos.filter((it) => it.videoId === item.videoId)[0];
        if (itemIsDisplay) {
            this.toastr.error('Video cant be removed because is marked for display!', 'Ooops!' , { disableTimeOut: true });
            return;
        }
        const itemIsRepeatedInDatabase = this.videosAll.filter((it) => it.videoId === item.videoId);
        if (itemIsRepeatedInDatabase.length > 1 || !itemIsDisplay) {
            for (let i = this.videosAll.length - 1; i >= 0; i--) {
                if (this.videosAll[i].videoId === item.videoId) {
                    // this.videosAll.splice(i, 1);
                    const initialState = {
                        display: { title: 'Are you sure you want to remove the video ' + this.videosAll[i].videoId }
                    };
                    const modal = this.modalService.show(AlertPopupComponent, Object.assign({}, this.modalConfig, { initialState }));
                    modal.content.onUserConfirm.subscribe((value) => {
                        if (value) {
                            this.deleteVideo(this.videosAll[i], this.videosAll[i]._id);
                            return;
                        }
                    });
                    break;
                }
            }
        }
    }
    deleteVideo(item, id) {
        this.cms.deleteVideo(item, id)
            .subscribe((data) => {
                this.fetchVideos();
                this.toastr.success('Video removed from database!', 'Video removed!', { disableTimeOut: true });
            });
    }
    fetchVideos() {
        this.cms.getVideos()
            .subscribe((data) => {
                this.videosAll = data;
            });
    }
    updateOrder() {
        this.cms.updateContent({
            videos: this.videos,
        }, this.content._id).subscribe((data) => {
            this.toastr.success('Video content is now up to date!', 'Update content success!', { disableTimeOut: true });
        }, (error) => {
            console.log(error);
            if (error.status === 422) {
                this.error = true;
                this.errorMessage = error.error.message;
            }
        });
    }
}
