import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CmsService } from '../../../shared/services/cms/cms.service';

@Component({
    selector: 'arf-content',
    templateUrl: './content.component.html',
    styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
    form: FormGroup;
    content: any = {};
    isEdit = false;
    error = false;
    errorMessage;
    
    constructor(private formBuilder: FormBuilder,
                private cms : CmsService,
                private toastr: ToastrService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'firstName': [ [Validators.required]],
            'lastName': [ [Validators.required]],
            'otherName': [ [Validators.required]],
            'title': [ [Validators.required]],
            'bio': [ [Validators.required]]
        });
        this.cms.getContent().subscribe((data) => {
            this.content = data[0];
            console.log(data);
           this.form.controls['firstName'].setValue(this.content['firstName']);
           this.form.controls['lastName'].setValue(this.content['lastName']);
           this.form.controls['otherName'].setValue(this.content['otherName']);
           this.form.controls['title'].setValue(this.content['title']);
           this.form.controls['bio'].setValue(this.content['bio']);
           
        });
    }
    
    updateContent() {
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({ 'required': true });
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        this.error = false;
        this.cms.updateContent({
            firstName: this.form.controls['firstName'].value,
            lastName: this.form.controls['lastName'].value,
            otherName: this.form.controls['otherName'].value,
            title: this.form.controls['title'].value,
            bio: this.form.controls['bio'].value,
        }, this.content._id).subscribe((data) => {
            this.toastr.success('Content is now up to date!', 'Update content success!' , { disableTimeOut: true });
            this.isEdit = false;
        }, (error) => {
            console.log(error);
            if (error.status === 422) {
                this.error = true;
                this.errorMessage = error.error.message;
            }
        });
    }
}
