import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CmsService } from '../../../shared/services/cms/cms.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';

function handleCallback(data) {
    console.log(data);
}

@Component({
    selector: 'arf-video-form',
    templateUrl: './video-form.component.html',
    styleUrls: ['./video-form.component.scss']
})
export class VideoFormComponent implements OnInit {
    id;
    video;
    error;
    errorMessage;
    isEdit = false;
    pic;
    form: FormGroup;
    videoType;
    
    constructor(private route: ActivatedRoute,
                private http: HttpClient,
                private formBuilder: FormBuilder,
                private cms: CmsService,
                private zone: NgZone,
                private toastr: ToastrService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'videoId': ['', [Validators.required]],
            'title': ['', [Validators.required]]
        });
        this.id = this.route.snapshot.paramMap.get('id');
        if (this.id) {
            this.fetchData();
        } else {
           console.log('is add');
           this.isEdit = true;
        }
    }
    
    fetchData() {
        this.cms.getVideoById(this.id)
            .subscribe((data) => {
                this.video = data;
                this.videoType = this.video.type;
                this.pic = this.video.pic;
                this.form.controls['videoId'].setValue(this.video['videoId']);
                this.form.controls['title'].setValue(this.video['title']);
            });
    }
    
    
    updateImage() {
        if (this.videoType === 'y') {
            try {
                this.pic = 'http://img.youtube.com/vi/' + this.form.controls['videoId'].value + '/hqdefault.jpg';
            } catch (e) {
                console.log(e);
            }
        }
        if (this.videoType === 'v') {
            this.http.get('https://vimeo.com/api/v2/video/' + this.form.controls['videoId'].value + '.json').toPromise()
                .then((data) => {
                    this.pic = data[0].thumbnail_large;
                }, (err) => {
                    console.log(err);
                });
        }
    }
    updateVideo() {
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({ 'required': true });
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        this.error = false;
        let _video;
        if (this.videoType === 'y') {
           _video = {
               videoType: this.videoType,
               videoId: this.form.controls['videoId'].value,
               iframe: 'https://www.youtube.com/embed/' + this.form.controls['videoId'].value,
               pic: 'http://img.youtube.com/vi/' + this.form.controls['videoId'].value + '/hqdefault.jpg',
               title: this.form.controls['title'].value
           };
        }
        if (this.videoType === 'v') {
            _video = {
                videoType: this.videoType,
                videoId: this.form.controls['videoId'].value,
                iframe: 'https://player.vimeo.com/video/' + this.form.controls['videoId'].value,
                pic: this.pic,
                title: this.form.controls['title'].value
            };
        }
        if (this.id) {
            this.cms.updateVideo(_video, this.video._id).subscribe((data) => {
                this.toastr.success('Content is now up to date!', 'Update video success!', { disableTimeOut: true });
                this.isEdit = false;
            }, (error) => {
                console.log(error);
                if (error.status === 422) {
                    this.error = true;
                    this.errorMessage = error.error.message;
                }
            });
        } else {
            this.cms.createVideo(_video).subscribe((data) => {
                this.toastr.success('New video added to the system!', 'Create video success!', { disableTimeOut: true });
                this.isEdit = false;
            }, (error) => {
                console.log(error);
                if (error.status === 422) {
                    this.error = true;
                    this.errorMessage = error.error.message;
                }
            });
        }
    }
    toggleVideo(e) {
        this.videoType = e;
    }
    
    toogleEdit() {
        this.isEdit = !this.isEdit;
        if (!this.isEdit) {
        
        }
    }
}
