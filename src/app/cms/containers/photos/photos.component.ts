import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CmsService } from '../../../shared/services/cms/cms.service';
import { CdkDragDrop, copyArrayItem, moveItemInArray } from '@angular/cdk/drag-drop';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PhotoItemComponent } from '../../components/photo-item/photo-item.component';
import { ToastrService } from 'ngx-toastr';
import { AlertPopupComponent } from '../../../shared/components/alert-popup/alert-popup.component';
import { BsModalService } from 'ngx-bootstrap';

@Component({
    selector: 'arf-photos',
    templateUrl: './photos.component.html',
    styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
    form: FormGroup;
    displayForm = false;
    itemToUpdate;
    @ViewChildren(PhotoItemComponent) photosList: QueryList<PhotoItemComponent>;
    pic;
    content;
    photos;
    private modalConfig = {
        keyboard: false,
        backdrop: true,
        ignoreBackdropClick: true,
    };
    constructor(public cms: CmsService, public formBuilder: FormBuilder,
                public modalService: BsModalService,
                public toastr: ToastrService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'description': [[Validators.required]]
        });
        
        this.cms.getContent()
            .subscribe((data) => {
                this.content = data[0];
                this.photos = this.content.photos || [];
            });
        this.fetchPhotos();
    }
    
    fetchPhotos() {
        this.cms.getPhotos()
            .subscribe((data) => {
                this.photos = data;
            });
    }
    fetchPhotosAndUpdate() {
        this.cms.getPhotos()
            .subscribe((data) => {
                this.photos = data;
                this.updateContent();
            });
    }
    
    onDrop(event: CdkDragDrop<string[]>) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
        // this.toastr.warning('Please update your changes!', 'Reorder success!');
        
    }
    
    showEditForm(item) {
        this.photosList.forEach(it => {
            it.isOnEdit = true;
           if (item._id === it.item._id) {
               it.isOnEdit = false;
           }
        });
            // return;
        this.itemToUpdate = item;
        this.displayForm = true;
        this.pic = 'assets/photos/' + item.image;
        this.form.controls['description'].setValue(item.description);
    }
    
    cancelEdit() {
        this.photosList.forEach(item => {
            item.isOnEdit = false;
        });
        this.itemToUpdate = null;
        this.displayForm = false;
    }
    
    updatePhoto() {
        const _photo = {
            description: this.form.controls['description'].value
        };
        this.cms.updatePhoto(_photo, this.itemToUpdate._id)
            .subscribe(
                (data) => {
                    this.fetchPhotosAndUpdate();
                },
                (err) => {
                    this.toastr.error('Error updating the photo', 'Update photo error!', { disableTimeOut: true });
                }
            );
    }
    removeFromDatabase(item) {
        this.cancelEdit();
        const initialState = {
            display: { title: 'Are you sure you want to remove the photo ' + item.image }
        };
        const modal = this.modalService.show(AlertPopupComponent, Object.assign({}, this.modalConfig, { initialState }));
        modal.content.onUserConfirm.subscribe((value) => {
            if (value) {
                this.deletePhoto(item);
                return;
            }
        });
    }
    deletePhoto(item) {
        this.cms.deletePhoto(item, item._id)
            .subscribe((data) => {
                this.fetchPhotosAndUpdate();
            },(err) => {
                this.toastr.error('Error deleting the photo', 'Delete photo error!', { disableTimeOut: true });
            });
    }
    updateContent() {
        this.cms.updateContent({
            photos: this.photos,
        }, this.content._id).subscribe((data) => {
            this.toastr.success('Photos content is now up to date!', 'Update content success!', { disableTimeOut: true });
            this.cancelEdit();
        }, (error) => {
            console.log(error);
        });
    }
}
