import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../shared/services/user.service/user.service';
import { AuthService } from '../../../shared/services/auth/auth-jwt.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'arf-cms',
  templateUrl: './cms.component.html',
  styleUrls: ['./cms.component.scss']
})
export class CmsComponent implements OnInit {
    
    user;
    constructor(private router: Router, private authService: AuthService,
                private $localStorage: LocalStorageService,
                private userService: UserService) { }
    
    ngOnInit() {
        this.user = this.$localStorage.retrieve('user');
    }
    logout() {
        this.userService.logout().subscribe(() => {
            this.authService.destroyAuth()
                .subscribe(() => {
                    this.router.navigate(['/login']);
                });
        });
    }

}
