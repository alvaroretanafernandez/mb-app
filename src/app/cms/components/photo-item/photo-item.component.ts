import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'arf-photo-item',
  templateUrl: './photo-item.component.html',
  styleUrls: ['./photo-item.component.scss']
})
export class PhotoItemComponent implements OnInit {
    @Input() item;
    @Input() index;
    @Output() onRemove = new EventEmitter();
    @Output() onEdit = new EventEmitter();
    @Input() isOnEdit = false;
    constructor() { }
    
    ngOnInit() {
    }
    remove(item) {
        this.onRemove.emit(item);
    }
    edit(item) {
        this.onEdit.emit(item);
    }

}
