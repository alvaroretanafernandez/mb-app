import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'arf-video-item',
  templateUrl: './video-item.component.html',
  styleUrls: ['./video-item.component.scss']
})
export class VideoItemComponent implements OnInit {
  @Input() item;
  @Input() index;
  @Input() color = 'light';
  @Output() onRemove = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }
  
  remove(item) {
    this.onRemove.emit(item);
  }
}
