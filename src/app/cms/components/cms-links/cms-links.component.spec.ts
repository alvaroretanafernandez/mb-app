import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CmsLinksComponent } from './cms-links.component';

describe('CmsLinksComponent', () => {
  let component: CmsLinksComponent;
  let fixture: ComponentFixture<CmsLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CmsLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CmsLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
