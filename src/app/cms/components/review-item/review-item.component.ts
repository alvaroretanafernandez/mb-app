import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'arf-review-item',
  templateUrl: './review-item.component.html',
  styleUrls: ['./review-item.component.scss']
})
export class ReviewItemComponent implements OnInit {
    @Input() item;
    @Output() onRemove = new EventEmitter();
    @Output() onEdit = new EventEmitter();
    @Input() isOnEdit = false;
    constructor() { }
    ngOnInit() {
    }
    remove(item) {
        this.onRemove.emit(item);
    }
    edit(item) {
        this.onEdit.emit(item);
    }

}
