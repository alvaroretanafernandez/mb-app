import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollapseModule } from 'ngx-bootstrap/collapse/collapse.module';
import { BsDropdownModule, ModalModule, TooltipModule } from 'ngx-bootstrap';

@NgModule({
    imports: [
        CommonModule,
        CollapseModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        BsDropdownModule.forRoot()
    ],
    exports: [
        CommonModule,
        CollapseModule,
        TooltipModule,
        ModalModule,
        BsDropdownModule
    ],
})
export class SharedLibModule {
}
