import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserRouteAccessService } from './shared/services/auth/user-route-access-service';
import { NoContentComponent } from './components/no-content/no-content.component';
import { HomeComponent } from './components/home/home.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login',  component: LoginComponent},
  { path: 'register',  component: RegisterComponent},
  { path: 'dashboard',  component: DashboardComponent, canActivate: [UserRouteAccessService] ,  data: { role: ['user', 'admin'] } },
  { path: 'account', loadChildren: './account/account.module#AccountModule'},
  { path: 'cms', loadChildren: './cms/cms.module#CmsModule'},
  { path: '**',    component: NoContentComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

