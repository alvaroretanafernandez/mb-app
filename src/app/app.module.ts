import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './core/containers/app.component';
import { LoginComponent } from './components/login/login.component';
import { CoreModule } from './core/core.module';
import { UserService } from './shared/services/user.service/user.service';
import { AuthService } from './shared/services/auth/auth-jwt.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { HttpsRequestInterceptor } from './shared/services/http-interceptor/http-interceptor';
import { LocalStorageService } from 'ngx-webstorage';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import { RegisterComponent } from './components/register/register.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserRouteAccessService } from './shared/services/auth/user-route-access-service';
import { ArfToastrComponent } from './shared/components/toastr/arf-toastr.component';
import { NoContentComponent } from './components/no-content/no-content.component';
import { SharedLibModule } from './shared-lib/shared-lib.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './components/home/home.component';
import { HomeModule } from './home/home.module';



@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        DashboardComponent,
        NoContentComponent,
        HomeComponent
    ],
    imports: [
        CommonModule,
        BrowserModule,
        NoopAnimationsModule,
        BrowserAnimationsModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        CoreModule,
        HomeModule,
        SharedModule,
        SharedLibModule,
        ToastrModule.forRoot({
            toastComponent: ArfToastrComponent,
            preventDuplicates: true
        }),
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpsRequestInterceptor,
            multi: true
        },
        LocalStorageService,
        UserRouteAccessService,
        UserService,
        AuthService],
    entryComponents: [ArfToastrComponent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
