import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'arf-subheader',
    templateUrl: './subheader.component.html',
    styleUrls: ['./subheader.component.scss'],
})
export class SubheaderComponent implements OnInit {
    _title: string;
    @Input('title')
    set title(value: string) {
        this._title = value;
    }
    get title(): string {
        return this.capitalizeFirstLetter(this._title);
    }
    constructor() {
    }

    ngOnInit() {
    }
    capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}
