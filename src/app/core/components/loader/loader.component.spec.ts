import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreModule} from '../../core.module';
import {LoaderComponent} from './loader.component';

describe('CoreModule - LoaderComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterTestingModule],
            declarations: [],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(LoaderComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
