import {Component, ViewChild} from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {NavbarComponent} from './navbar/navbar.component';

@Component({
    selector: 'arf-root',
    templateUrl: './app.component.html'
})
export class AppComponent {
    title = 'Matt Brock - Composer London - Music';
    public isCollapsed = true;
    public loading = true;
    @ViewChild('nav') public nav: NavbarComponent;

    constructor(public router: Router) {
        router.events.subscribe((event: any) => {
            this.navigationInterceptor(event);
        });
    }

    navigationInterceptor(event: any): void {
        if (event instanceof NavigationStart) {
            this.loading = true;
        }
        if (event instanceof NavigationEnd) {
            this.loading = false;
            this.nav.isCollapsed = true;
            document.body.scrollTop = 0;
            // this.title = this.router.url.split('/')[1];
        }
        if (event instanceof NavigationCancel || event instanceof NavigationError) {
            this.loading = false;
        }
    }

}
