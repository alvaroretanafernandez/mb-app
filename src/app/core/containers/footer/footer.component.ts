import {
    Component,
    OnInit
} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'arf-footer',
    styleUrls: ['./footer.component.scss'],
    templateUrl: './footer.component.html'
})
export class FooterComponent implements OnInit {

    footerMenu = environment.content.global.menu;
    constructor() {
    }
    public ngOnInit() {
    }
}
