import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreModule} from '../../core.module';
import {FooterComponent} from './footer.component';

describe('Module - Core - FooterComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterTestingModule],
            declarations: [],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(FooterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
