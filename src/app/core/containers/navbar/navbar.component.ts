import {
    Component, Input,
    OnInit
} from '@angular/core';
import { environment } from '../../../../environments/environment';
import { AuthService } from '../../../shared/services/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { UserService } from '../../../shared/services/user.service/user.service';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
    selector: 'arf-navbar',
    styleUrls: ['./navbar.component.css'],
    templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
    @Input() public isCollapsed;
    menu: any = environment.content.global.menu;
    user;
    menuAuth: any = environment.content.global.menuAuthNav;
    isLoggedIn = false;
    constructor(private router: Router, private authService: AuthService, private userService: UserService, private $localStorage: LocalStorageService) {
        this.authService.getAuthenticationState().subscribe((user) => {
            this.user = user;
            this.isLoggedIn = user !== null;
        });
    }
    public ngOnInit() {
        this.user = this.$localStorage.retrieve('user');
        this.authService.isLoggedIn().then((res) => this.isLoggedIn = res);
    }
    logout() {
        this.userService.logout().subscribe(() => {
            this.authService.destroyAuth()
                .subscribe(() => {
                    this.router.navigate(['/login']);
                });
        }, err => {
            console.log(err);
        });
    }
}
