import {TestBed, async} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {NavbarComponent} from './navbar.component';
import {CoreModule} from '../../core.module';
import { AuthService } from '../../../shared/services/auth/auth-jwt.service';
import { UserService } from '../../../shared/services/user.service/user.service';
import { LocalStorageService } from 'ngx-webstorage';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('Module - Core - NavbarComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [CoreModule, RouterTestingModule, HttpClientTestingModule],
            declarations: [],
            providers:[AuthService, UserService, LocalStorageService]
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(NavbarComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
