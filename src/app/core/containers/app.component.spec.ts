import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {RouterTestingModule} from '@angular/router/testing';
import {CoreModule} from '../core.module';
import {AppModule} from '../../app.module';

describe('App', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, CoreModule, RouterTestingModule],
      declarations: [],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'stoked'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Client App');
  });

});
