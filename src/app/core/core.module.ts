/* tslint:disable:member-ordering no-unused-variable */
import {
  NgModule,
  Optional, SkipSelf
} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './containers/navbar/navbar.component';
import {FooterComponent} from './containers/footer/footer.component';
import {LoaderComponent} from './components/loader/loader.component';
import {AppComponent} from './containers/app.component';
import {SharedLibModule} from '../shared-lib/shared-lib.module';
import {SharedModule} from '../shared/shared.module';
import { SubheaderComponent } from './components/subheader/subheader.component';

@NgModule({
  imports: [
      CommonModule,
      RouterModule,
      SharedLibModule,
      SharedModule
  ],
  declarations: [
      AppComponent,
      NavbarComponent,
      SubheaderComponent,
      LoaderComponent,
      FooterComponent,
  ],
  exports: [
      AppComponent,
      NavbarComponent,
      SubheaderComponent,
      FooterComponent,
      LoaderComponent,
      // modules
      CommonModule,
      RouterModule,
      SharedLibModule,
      SharedModule
  ],
})
export class CoreModule {
}

