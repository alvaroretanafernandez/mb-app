import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentTitleComponent } from './components/content-title/content-title.component';
import { ContentSubTitleComponent } from './components/content-sub-title/content-sub-title.component';
import { ContentPhotoItemComponent } from './components/content-photos/photo-item/content-photo-item.component';
import { ContentVideosComponent } from './components/content-videos/content-videos.component';
import { ContentReviewComponent } from './components/content-review/content-review.component';
import { ContentPhotosComponent } from './components/content-photos/content-photos.component';
import { ContentBioComponent } from './components/content-bio/content-bio.component';
import { ContentContactComponent } from './components/content-contact/content-contact.component';
import { SocialComponent } from './components/social/social.component';
import { SafePipe } from '../shared/pipes/safe.pipe';

@NgModule({
    imports: [
        CommonModule,
    ],
    declarations: [
        SafePipe,
        ContentTitleComponent,
        ContentBioComponent,
        ContentReviewComponent,
        ContentPhotosComponent,
        ContentVideosComponent,
        ContentSubTitleComponent,
        ContentContactComponent,
        ContentPhotoItemComponent,
        SocialComponent
    ],
    exports: [
        CommonModule,
        ContentTitleComponent,
        ContentBioComponent,
        ContentReviewComponent,
        ContentPhotosComponent,
        ContentVideosComponent,
        ContentSubTitleComponent,
        ContentContactComponent,
        ContentPhotoItemComponent,
        SocialComponent,
        SafePipe
    ],
})
export class HomeModule {
}
