import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentSubTitleComponent } from './content-sub-title.component';

describe('ContentSubTitleComponent', () => {
  let component: ContentSubTitleComponent;
  let fixture: ComponentFixture<ContentSubTitleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentSubTitleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentSubTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
