import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'arf-content-sub-title',
  templateUrl: './content-sub-title.component.html',
  styleUrls: ['./content-sub-title.component.scss']
})
export class ContentSubTitleComponent implements OnInit {
  @Input() title;
  @Input() position = 'center';
  constructor() { }

  ngOnInit() {
  }

}
