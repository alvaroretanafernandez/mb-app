import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'arf-content-photo-item',
  templateUrl: './photo-item.component.html',
  styleUrls: ['./photo-item.component.scss']
})
export class ContentPhotoItemComponent implements OnInit {
    @Input() photo;
  constructor() { }

  ngOnInit() {
  }

}
