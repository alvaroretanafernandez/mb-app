import { Component, Input, OnInit } from '@angular/core';
import { PopupComponent } from '../../../shared/components/popup/popup.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { animate, query, stagger, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'arf-content-photos',
  templateUrl: './content-photos.component.html',
  styleUrls: ['./content-photos.component.scss'],
    animations: [
        trigger('photosAnimationVertical', [
            transition('* => *', [
                query('arf-photo-item', style({ transform: 'translateY(300%)', opacity: 0.6, width: '0%'}),{optional: true}),
                query('arf-photo-item',
                    stagger('22ms', [
                        animate('1200ms', style({ transform: 'translateY(0)', opacity: 1, width: '100%'}))
                    ]), {optional: true})
            ])
        ])
    ]
})
export class ContentPhotosComponent implements OnInit {
  @Input() photos$;
  modalRef: BsModalRef;
  constructor(public modalService: BsModalService,) { }

  ngOnInit() {
  }
    openModal(photo) {
        const initialState = {
            display: photo
        };
        this.modalRef = this.modalService.show(PopupComponent, { initialState, class: 'modal-lg' });
    }
}
