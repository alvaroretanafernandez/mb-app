import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PopupComponent } from '../../../shared/components/popup/popup.component';

@Component({
  selector: 'arf-content-videos',
  templateUrl: './content-videos.component.html',
  styleUrls: ['./content-videos.component.scss']
})
export class ContentVideosComponent implements OnInit {
    
    @Input() videos$;
    modalRef: BsModalRef;
    constructor(public modalService: BsModalService) { }
    
    ngOnInit() {
    }
    openModal(photo) {
        const initialState = {
            display: photo,
            iframe : true
        };
        this.modalRef = this.modalService.show(PopupComponent, { initialState, class: 'modal-lg' });
    }

}
