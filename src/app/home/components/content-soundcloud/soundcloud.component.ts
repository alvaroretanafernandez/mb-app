import { AfterViewInit, Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';

@Component({
    selector: 'arf-soundcloud',
    templateUrl: './soundcloud.component.html',
    styleUrls: ['./soundcloud.component.scss']
})
export class SoundcloudComponent implements OnInit, AfterViewInit {
    @ViewChild('wrap') iframeWrap: ElementRef;
    iframeWidth;
    iframeHeight;
    @HostListener('window:resize', ['$event.target'])
    onResize() {
        this.resizeWorks();
    }
    constructor() {
    }
    ngOnInit() {
        this.iframeWidth = this.iframeWrap.nativeElement.offsetWidth - 15;
        this.iframeHeight = this.iframeWidth / 1.45;
    }
    ngAfterViewInit() {
    }
    private resizeWorks(): void {
        this.iframeWidth = this.iframeWrap.nativeElement.offsetWidth;
        this.iframeHeight = this.iframeWidth / 1.45;
    }
}
