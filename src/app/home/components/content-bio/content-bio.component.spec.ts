import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentBioComponent } from './content-bio.component';

describe('ContentBioComponent', () => {
  let component: ContentBioComponent;
  let fixture: ComponentFixture<ContentBioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentBioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentBioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
