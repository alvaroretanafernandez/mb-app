import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'arf-content-bio',
    templateUrl: './content-bio.component.html',
    styleUrls: ['./content-bio.component.scss']
})
export class ContentBioComponent implements OnInit {
    @Input() content;
    
    constructor() {
    }
    
    ngOnInit() {
    }
    
}
