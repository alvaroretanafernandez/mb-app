import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'arf-content-review',
  templateUrl: './content-review.component.html',
  styleUrls: ['./content-review.component.scss']
})
export class ContentReviewComponent implements OnInit {
  @Input() reviews;
  constructor() { }

  ngOnInit() {
  }

}
