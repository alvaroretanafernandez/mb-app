import {TestBed, async} from '@angular/core/testing';
import {PreFooterComponent} from './prefooter.component';
import {RouterTestingModule} from '@angular/router/testing';
import {SocialComponent} from '../social/social.component';

describe('Module - Core - PreFooterComponent', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [
                PreFooterComponent,
                SocialComponent
            ],
        }).compileComponents();
    });

    it('should create the component', () => {
        const fixture = TestBed.createComponent(PreFooterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
