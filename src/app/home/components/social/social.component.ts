import {Component, Input, OnInit} from '@angular/core';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'arf-social',
    templateUrl: './social.component.html',
    styleUrls: ['./social.component.css']
})
export class SocialComponent implements OnInit {
    social: any = environment.content.global.social;
    @Input() size = 4;
    constructor() {
    }

    ngOnInit() {
    }

}
