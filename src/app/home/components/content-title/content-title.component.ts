import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'arf-content-title',
  templateUrl: './content-title.component.html',
  styleUrls: ['./content-title.component.scss']
})
export class ContentTitleComponent implements OnInit {
  @Input() content;
  constructor() { }

  ngOnInit() {
  }

}
