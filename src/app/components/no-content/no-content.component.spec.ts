import { TestBed, async } from '@angular/core/testing';
import {NoContentComponent} from './no-content.component';

describe('App - no-content-component', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [
                NoContentComponent
            ],
        }).compileComponents();
    });

    it('App - no-content-component - should create the component', () => {
        const fixture = TestBed.createComponent(NoContentComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
