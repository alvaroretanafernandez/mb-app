import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { Observable } from 'rxjs/Observable';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AppModule } from '../../app.module';
import { Component, Injectable, NgModuleFactoryLoader } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { LocalStorageService } from 'ngx-webstorage';
import { of } from 'rxjs';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';

const mockUser = {_id: '5a56c5df49337d08513fcbc4',
  displayName: 'alvaro retana',
  provider: 'local',
  created: '2018-01-11T02:03:11.471Z',
  roles: ['user', 'admin'],
  profileImageURL: 'modules/users/client/img/profile/default.png',
  email: 'arfs@ecolove.com',
  lastName: 'retana',
  firstName: 'alvaro'
};

const mockToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTU2YzVkZjQ5MzM3ZDA4NTEzZmNiYzQiLCJka' +
  'XNwbGF5TmFtZSI6ImFsdmFybyByZXRhbmEiLCJwcm92aWRlciI6ImxvY2FsIiwiX192IjowLCJjcmVhdGVkIjoiMjAxOC0wMS0xMV' +
  'QwMjowMzoxMS40NzFaIiwicm9sZXMiOlsidXNlciIsImFkbWluIl0sInByb2ZpbGVJbWFnZVVSTCI6Im1vZHVsZXMvdXNlcnMvY2xpZ' +
  'W50L2ltZy9wcm9maWxlL2RlZmF1bHQucG5nIiwiZW1haWwiOiJhcmZzQGVjb2xvdmUuY29tIiwibGFzdE5hbWUiOiJyZXRhbmEiLCJmaX' +
  'JzdE5hbWUiOiJhbHZhcm8iLCJpYXQiOjE1MTY0Njk4ODQsImV4cCI6MTcyOTQ5NDU4ODR9.sA6fLQgPWiIKgnpEOfXvs9dvZLEWLMS_1eMbgwJvoUg';

@Injectable()
class MockAuthService {
  constructor(private $localStorage: LocalStorageService) {}
  setAuth(user, token): Promise<any> {
    this.$localStorage.store('authenticationToken', mockToken);
    this.$localStorage.store('user', mockUser);
    return Promise.resolve();
  }
}

class MockUserService {
  login(credentials: any): Observable<any> {
    console.log('yo');
    return of({data: {user: mockUser, token: mockToken}});
  }
}

@Component({template: 'mock-admin'})
class MockAdminComponent {

}

describe('App - login-component', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, SharedModule, RouterTestingModule.withRoutes([{
        path: 'dashboard',
        component: MockAdminComponent
      }])],
      declarations: [ MockAdminComponent],
      providers: [
        LocalStorageService,
        { provide: UserService, useClass: MockUserService },
        { provide: AuthService, useClass: MockAuthService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  afterEach(() => {
    const $localStorage = TestBed.get(LocalStorageService);
    $localStorage.clear('authenticationToken');
    $localStorage.clear('user');
  });
  it('App - login-component - should create the component', () => {
    expect(component).toBeTruthy();
  });
  it('App - login-component - should have authenticationError defined in the component', () => {
    fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.authenticationError).toBeFalsy();
  });
  it('App - login-component - should have credentials defined in the component', () => {
    fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.credentials).toBeTruthy();
  });
  it('App - login-component - should have password defined in the component', () => {
    fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.password).toBeFalsy();
  });
  it('App - login-component - should have email defined in the component', () => {
    fixture = TestBed.createComponent(LoginComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.email).toBeFalsy();
  });
  
});
