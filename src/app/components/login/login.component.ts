import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'arf-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    authenticationError = false;
    errorMessage;
    credentials: any = {};
    password: string;
    email: string;
    form: FormGroup;
    constructor(
        private router: Router,
        private formBuilder: FormBuilder,
        private toastr: ToastrService,
        private userService: UserService,
        private authService: AuthService) {
    }
    
    ngOnInit() {
        this.form = this.formBuilder.group({
            'email': ['alvaro@myemail.com', [Validators.required]],
            'password': ['', [Validators.required]]
        });
    }
    
    login() {
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({'required': true});
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        const credentials = {
            email: this.form.controls['email'].value,
            password: this.form.controls['password'].value,
        };
        this.userService.login(credentials)
            .subscribe(
            (data) => {
                this.authenticationError = false;
                this.authService.setAuth(data.user, data.token)
                    .then(() => {
                        setTimeout(() => {
                            this.router.navigate(['dashboard'])
                                .then(() => {
                                    // 2. show toaster
                                    this.toastr.success('You are now logged in!', 'Register user success!' , { disableTimeOut: true });
                                });
                        }, 500);
                    });
            },
            (error) => {
                console.log(error);
                if (error.status === 422) {
                    this.authenticationError = true;
                    this.errorMessage = error.error.message;
                }
                // this.toastr.error('Login Error!', 'You are not logged in');
                // this.authenticationError = true;
            });
    }
}
