import { Component, OnInit, TemplateRef } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { PopupComponent } from '../../shared/components/popup/popup.component';
import { environment } from '../../../environments/environment';
import { CmsService } from '../../shared/services/cms/cms.service';

@Component({
    selector: 'arf-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    
    photos$;
    videos$;
    reviews$;
    content;
    modalRef: BsModalRef;
    
    constructor(private http: HttpClient, private modalService: BsModalService, private cms: CmsService) {
    }
    
    ngOnInit() {
        this.cms.getContent().subscribe((data) => {
            this.content = data[0];
            this.videos$ = this.content.videos;
            this.photos$ = this.content.photos;
            this.reviews$ = this.content.reviews;
        });
    }
    openModal(photo) {
        const initialState = {
            display: photo
        };
        this.modalRef = this.modalService.show(PopupComponent, { initialState, class: 'modal-lg' });
    }
}
