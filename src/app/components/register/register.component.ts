import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'arf-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    confirmPassword: string;
    errorMessage: string;
    error;
    success = false;
    form: FormGroup;
    
    constructor(private toastr: ToastrService,
                private router: Router,
                private formBuilder: FormBuilder,
                private userService: UserService,
                private authService: AuthService) {
    }
    ngOnInit() {
        this.success = false;
        this.form = this.formBuilder.group({
            'firstName': ['alvaro', [Validators.required]],
            'lastName': ['retana', [Validators.required]],
            'userName': ['myusername', [Validators.required]],
            'email': ['alvaro@myemail.com', [Validators.required]],
            'password': ['', [Validators.required]]
        });
    }
    register() {
        if (this.form.invalid) {
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    if (this.form.controls[control].invalid) {
                        this.form.controls[control].setErrors({ 'required': true });
                        this.form.controls[control].markAsDirty();
                        this.form.controls[control].markAsTouched();
                    }
                }
            }
            return;
        }
        this.error = false;
        this.userService.register({
            firstName: this.form.controls['firstName'].value,
            lastName: this.form.controls['lastName'].value,
            username: this.form.controls['userName'].value,
            email: this.form.controls['email'].value,
            password: this.form.controls['password'].value,
        }).subscribe((data) => {
            this.success = true;
            this.authService.setAuth(data.user, data.token)
                .then(() => {
                    // 1. navigate to admin
                    this.router.navigate(['/dashboard'])
                        .then(() => {
                            // 2. show toaster
                            this.toastr.success('You are now logged in!', 'Register user success!' , { disableTimeOut: true });
                        });
                });
        }, (error) => {
            console.log(error);
            if (error.status === 422) {
                this.error = true;
                this.errorMessage = error.error.message;
            }
        });
    }
}
