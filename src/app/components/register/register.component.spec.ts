import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterComponent } from './register.component';
import { Component, Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Observable } from 'rxjs/Observable';
import { AppModule } from '../../app.module';
import { SharedModule } from '../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../shared/services/user.service/user.service';
import { AuthService } from '../../shared/services/auth/auth-jwt.service';
import { of } from 'rxjs';

const mockUser = {
    _id: '5a56c5df49337d08513fcbc4',
    displayName: 'alvaro retana',
    provider: 'local',
    created: '2018-01-11T02:03:11.471Z',
    roles: ['user', 'admin'],
    profileImageURL: 'modules/users/client/img/profile/default.png',
    email: 'arfs@ecolove.com',
    lastName: 'retana',
    firstName: 'alvaro'
};

const mockToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YTU2YzVkZjQ5MzM3ZDA4NTEzZmNiYzQiLCJka' +
    'XNwbGF5TmFtZSI6ImFsdmFybyByZXRhbmEiLCJwcm92aWRlciI6ImxvY2FsIiwiX192IjowLCJjcmVhdGVkIjoiMjAxOC0wMS0xMV' +
    'QwMjowMzoxMS40NzFaIiwicm9sZXMiOlsidXNlciIsImFkbWluIl0sInByb2ZpbGVJbWFnZVVSTCI6Im1vZHVsZXMvdXNlcnMvY2xpZ' +
    'W50L2ltZy9wcm9maWxlL2RlZmF1bHQucG5nIiwiZW1haWwiOiJhcmZzQGVjb2xvdmUuY29tIiwibGFzdE5hbWUiOiJyZXRhbmEiLCJmaX' +
    'JzdE5hbWUiOiJhbHZhcm8iLCJpYXQiOjE1MTY0Njk4ODQsImV4cCI6MTcyOTQ5NDU4ODR9.sA6fLQgPWiIKgnpEOfXvs9dvZLEWLMS_1eMbgwJvoUg';

@Injectable()
class MockAuthService {
    constructor(private $localStorage: LocalStorageService) {
    }
    setAuth(user, token): Promise<any> {
        this.$localStorage.store('authenticationToken', mockToken);
        this.$localStorage.store('user', mockUser);
        return Promise.resolve();
    }
}

class MockUserService {
    register(credentials: any): Observable<any> {
        console.log('yo');
        return of({ data: { user: mockUser, token: mockToken } });
    }
}

@Component({ template: 'mock-admin' })
class MockAdminComponent {
}

describe('App - register-component', () => {
    let component: RegisterComponent;
    let fixture: ComponentFixture<RegisterComponent>;
    
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [AppModule, SharedModule, RouterTestingModule.withRoutes([{
                path: 'admin',
                component: MockAdminComponent
            }])],
            declarations: [MockAdminComponent],
            providers: [
                LocalStorageService,
                { provide: UserService, useClass: MockUserService },
                { provide: AuthService, useClass: MockAuthService }
            ]
        })
            .compileComponents();
    }));
    
    beforeEach(() => {
        fixture = TestBed.createComponent(RegisterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    afterEach(() => {
        const $localStorage = TestBed.get(LocalStorageService);
        $localStorage.clear('authenticationToken');
        $localStorage.clear('user');
    });
    it('App - register-component - should create the component', () => {
        expect(component).toBeTruthy();
    });
    it('App - register-component - should have registerAccount defined in the component', () => {
        fixture = TestBed.createComponent(RegisterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.registerAccount).toBeUndefined();
    });
    it('App - register-component - should have confirmPassword defined in the component', () => {
        fixture = TestBed.createComponent(RegisterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.confirmPassword).toBeUndefined();
    });
    it('App - register-component - should have error defined in the component', () => {
        fixture = TestBed.createComponent(RegisterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.error).toBeUndefined();
    });
    it('App - register-component - should have doNotMatch defined in the component', () => {
        fixture = TestBed.createComponent(RegisterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.doNotMatch).toBeUndefined();
    });
    it('App - register-component - should have success defined in the component', () => {
        fixture = TestBed.createComponent(RegisterComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app.success).toBe(false);
    });
});
