import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { UserService } from '../../shared/services/user.service/user.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'arf-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user;
  item;
  today;
  constructor( private $localStorage: LocalStorageService, private us: UserService, private cdr: ChangeDetectorRef) {
  }
  ngOnInit() {
    this.user = this.$localStorage.retrieve('user');
    this.searchLocation();
  }
    searchLocation(query = 'london') {
    }
}
