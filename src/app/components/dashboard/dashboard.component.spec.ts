import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { WeatherService } from '../../shared/services/weather/weather.service';
import { LocalStorageService } from 'ngx-webstorage';
import { UserService } from '../../shared/services/user.service/user.service';
import { SharedModule } from '../../shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('DashboardComponent', () => {
    let component: DashboardComponent;
    let fixture: ComponentFixture<DashboardComponent>;
    
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports:[SharedModule, HttpClientTestingModule, RouterTestingModule],
            declarations: [DashboardComponent],
            providers: [WeatherService, LocalStorageService, UserService]
        })
            .compileComponents();
    }));
    
    beforeEach(() => {
        fixture = TestBed.createComponent(DashboardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    
    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
